SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE DATABASE IF NOT EXISTS `comp6002_group11_assessment3` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `comp6002_group11_assessment3`;

DROP TABLE IF EXISTS `tbl_admin`;
CREATE TABLE `tbl_admin` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(20) NOT NULL,
  `PASSWRD` varchar(20) NOT NULL,
  `CATEGORY` varchar(11),
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tbl_admin` (`ID`, `NAME`, `PASSWRD`, `CATEGORY`) VALUES
(1, 'admin', 'admin', '');
INSERT INTO `tbl_admin` (`ID`, `NAME`, `PASSWRD`, `CATEGORY`) VALUES
(2, 'a', 'a', '');

DROP TABLE IF EXISTS `tbl_nav`;
CREATE TABLE `tbl_nav` (
  `PAGE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `TITLE` varchar(256) NOT NULL,
  PRIMARY KEY (`PAGE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into tbl_nav (title) values ('Google Apps');
insert into tbl_nav (title) values ('Slack');
insert into tbl_nav (title) values ('Microsoft Imagine');
insert into tbl_nav (title) values ('Pandora');

DROP TABLE IF EXISTS `tbl_jumbo`;
CREATE TABLE `tbl_jumbo` (
  `PAGE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `TITLE` varchar(256) NOT NULL,
  `SUBTITLE` varchar(1000),
  `BUTTONTITLE` varchar(256),
  `BUTTONLINK` varchar(256),
  PRIMARY KEY (`PAGE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into tbl_jumbo (title, subtitle, buttontitle, buttonlink) values ('Google Apps', 'There are many Google Apps that will help with your studies', 'Google Apps', 'https://accounts.google.com/signin/v2/identifier?flowName=GlifWebSignIn&flowEntry=ServiceLogin');
insert into tbl_jumbo (title, subtitle, buttontitle, buttonlink) values ('Slack', 'Slack is a great application for communicating with students and teachers', 'Slack website', 'https://to-bcs.slack.com/');
insert into tbl_jumbo (title, subtitle, buttontitle, buttonlink) values ('Microsoft Imagine', 'Microsoft imagine gives students access to a number of Microsoft programs for free', 'Microsoft Imagine', 'https://e5.onthehub.com/WebStore/Security/Signin.aspx?ws=20830094-5e9b-e011-969d-0030487d8897&vsro=8&rurl=%2fWebStore%2fProductsByMajorVersionList.aspx%3fcmi_cs%3d1%26cmi_mnuMain%3dbdba23cf-e05e-e011-971f-0030487d8897%26ws%3d20830094-5e9b-e011-969d-0030487d8897%26vsro%3d8');
insert into tbl_jumbo (title, subtitle, buttontitle, buttonlink) values ('Pandora', 'Information about the Pandora network', 'Pandora Homepage', 'index.php');


DROP TABLE IF EXISTS `tbl_jumbo_index`;
CREATE TABLE `tbl_jumbo_index` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TITLE` varchar(256) NOT NULL,
  `SUBTITLE` varchar(1000),
  `BUTTONTITLE` varchar(256),
  `BUTTONLINK` varchar(256),
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into tbl_jumbo_index (title, subtitle, buttontitle, buttonlink) values ('Welcome to Pandora', 'This site will give you an introduction into what the Pandora network offers', 'Toi Ohomai Site', 'https://toiohomai.ac.nz/');


DROP TABLE IF EXISTS `tbl_quicklink`;
CREATE TABLE `tbl_quicklink` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `IMGSRC` varchar(1000) NOT NULL,
  `LINK` varchar(1000) NOT NULL,
  `ALT` varchar(256) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into tbl_quicklink (imgsrc, link, alt) values ('images/moodle-300x150.jpg', 'http://moodle2.boppoly.ac.nz', 'moodle');
insert into tbl_quicklink (imgsrc, link, alt) values ('images/slack-300x150.png', 'https://slack.com/', 'slack');
insert into tbl_quicklink (imgsrc, link, alt) values ('images/toi_300x150.png', 'https://toiohomai.ac.nz/', 'toiohomai');
insert into tbl_quicklink (imgsrc, link, alt) values ('images/gmail.png', 'https://www.google.com/gmail/', 'gmail');
insert into tbl_quicklink (imgsrc, link, alt) values ('images/gdrive.png', 'https://www.google.com/drive/', 'google drive');
insert into tbl_quicklink (imgsrc, link, alt) values ('images/msimagine.jpg', 'https://e5.onthehub.com/WebStore/Security/Signin.aspx?ws=20830094-5e9b-e011-969d-0030487d8897&vsro=8&rurl=%2fWebStore%2fProductsByMajorVersionList.aspx%3fws%3d20830094-5e9b-e011-969d-0030487d8897', 'micrsoft imagine');
insert into tbl_quicklink (imgsrc, link, alt) values ('images/waikatouni.jpg', 'http://www.waikato.ac.nz/', 'waikatouniversity');
insert into tbl_quicklink (imgsrc, link, alt) values ('images/bitbucket-300.png', 'https://bitbucket.org/', 'bitbucket');

DROP TABLE IF EXISTS `tbl_contentIndex`;
CREATE TABLE `tbl_contentIndex` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CONTENT` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into tbl_contentIndex (content) values ('<h1>Welcome to BCS.NET.NZ</h1>
                                                <p>Toi Ohomai Institute of Technology Bachelor of Computing and Mathematical Science<br /><br />
                                                This site is for students who are completing the Bachelor of Computer Science in Applied Science.<br /><br />
                                                On this little mini-site you can find information specific to the Pandora labs that are used as part of your course.<br /><br />
                                                You can also find information relating to helpful tools that will help you with your studies and special offers available to you as a student at Toi Ohomai. These include the Google Apps, Slack and Microsoft Imagine.
                                                </p>');
                                                                  

insert into tbl_contentIndex (content) values ('<h1>Student offers</h1><p>As a student of Toi Ohomai Institute of Technology, you gain benefits similar to those of students in other courses.<br /><br />
                                              Many of these benefits can be accessed through the <a class="helpful" href="https://getconnected.boppoly.ac.nz/go/myplace" target="_blank">getconnected</a> page on the main site, but below are some of the common benefits.</p>
                                              <ul>
                                                  <li><p>You can access the Toi Ohomai moodle site <a class="helpful" href="http://moodle2.boppoly.ac.nz/" target="_blank">here</a>. Moodle is a common tool used by students and teachers to share course work and upload/submit assessments.</p></li>
                                                  <li><p>Computing students at Toi Ohomai get free access to <a class="helpful" href="http://moodle2.boppoly.ac.nz/course/view.php?id=2976" target="_blank">Lynda.com</a>.<br />Lynda.com offers a broad range of video tutorials and courses relevant to you as Computing students.</p></li>
                                                  <li><p>Students can also download <a class="helpful" href="https://www.boppoly.ac.nz/go/office-365-students" target="_blank">Office 365</a> for free. Office 365 gives you access to Microsoft Word, PowerPoint, Excel and many more programs that will be beneficial throughout your studies.</p></li>
                                              </ul>');

insert into tbl_contentIndex (content) values ('<h1>Using the Student WiFi</h1>
                                              <p>At Toi Ohomai all students have access to the Student WiFi.<br />To connect to the Student WiFi follow these steps:</p>
                                              <ol>
                                                  <li><p>Select Toi Ohomai Wi-Fi in your Wifi Settings</p></li>
                                                  <li><p>Username = moodle-id@stu.boppoly.ac.nz</p></li>
                                                  <li><p>Password = your student id (unless you have changed it)</p></li>
                                              </ol>');  

insert into tbl_contentIndex (content) values ('<h1>Course Information</h1>
                                              <ul>
                                                  <li><p><a class="helpful" href="https://www.boppoly.ac.nz/go/programmes-and-courses/new-zealand-diploma-in-web-development-and-design-level-5?subject=63" target="_blank">New Zealand Diploma in Web Development and Design (Level 5)</a></p></li>
                                                  <li><p><a class="helpful" href="https://www.boppoly.ac.nz/go/programmes-and-courses/new-zealand-certificate-in-information-technology-level-5?subject=63" target="_blank">New Zealand Certificate in Information Technology (Level 5)</a></p></li>
                                                  <li><p><a class="helpful" href="https://www.boppoly.ac.nz/go/programmes-and-courses/new-zealand-diploma-in-software-development-level-6?subject=63" target="_blank">New Zealand Diploma in Software Development (Level 6)</a></p></li>
                                                  <li><p><a class="helpful" href="https://www.boppoly.ac.nz/go/programmes-and-courses/bachelor-science-bsc-majoring-in-applied-computing?subject=63" target="_blank">Bachelor of Science (BSc) Majoring in Applied Computing</a></p></li>
                                              </ul>
                                              <br>');    
                                              
DROP TABLE IF EXISTS `tbl_contentOther`;
CREATE TABLE `tbl_contentOther` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CONTENT` text NOT NULL,
  `PAGE_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;   

insert into tbl_contentOther (content, page_id) values ('<h1>Setting up your Google profile</h1>
                                                  <p>With chrome you can sign into your google account from the browser app or on the webpage.<br />
                                                  If you do it from the browser app, then it will create a link to your google profile and sign into your personal account and the school account at the same time.<br /><br /><br />
                                                  Have a look at this gif to see how to sign into Chrome. (Click the image to enlarge)</p>', 1);
                                                                  

insert into tbl_contentOther (content, page_id) values ('<p><img src="../images/chrome-signin.gif" alt="" height="300" width="500"></p>', 1);

insert into tbl_contentOther (content, page_id) values ('<h1>Setting up your Google Apps Account</h1>
                                                  <p>At the beginning of each semester, all the passwords are reset to 12345678<br /><br />
                                                  When you sign in  with Chrome you will need to know your student Id.<br /><br /></p>
                                                  <ul>
                                                      <li><p>Your Google Apps ID = student-id@bcs.net.nz</p></li>
                                                      <li><p>Your password is 12345678 (You will be asked to change this after you accept the terms and conditions)</p></li>
                                                  </ul>', 1);       

insert into tbl_contentOther (content, page_id) values ('<p><img src="../images/googleapps.png" alt="" height="300" width="500"></p> ', 1);                                                      

DROP TABLE IF EXISTS `tbl_pages`;
CREATE TABLE `tbl_pages` (
  `PAGE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `PAGE_NAME` varchar(256) NOT NULL,
  PRIMARY KEY (`PAGE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;     

insert into tbl_pages (page_id, page_name) values (1, 'GoogleApps');
insert into tbl_pages (page_id, page_name) values (2, 'Slack');
insert into tbl_pages (page_id, page_name) values (3, 'MicrosoftImagine');
insert into tbl_pages (page_id, page_name) values (4, 'Pandora'); 

insert into tbl_contentOther (content, page_id) values ('<h1>Setting up your Slack Account</h1>
                        <p>Within the IT Team and some of your courses we use slack to communicate.<br /><br />
                        To create your account simply go to <a class="helpful" href="https://to-bcs.slack.com" target="_blank">TO-IT Slack Page</a> and sign in with your Google Apps Account.<br /><br /><br />
                        This image will show you how to sign into Slack. (Click the image to enlarge)</p>', 2);
                                                                  

insert into tbl_contentOther (content, page_id) values ('<p><img src="../images/slack-signin.gif" alt="" height="300" width="500"></p>', 2);

insert into tbl_contentOther (content, page_id) values ('<h1>Download the Slack App for your device</h1>
                        <p>Slack also offers a downloadable Application for most devices.<br /><br />
                        <a class="helpful" href="https://slack.com/downloads" target="_blank">This</a> page will detect which system you are using and offer the download to suit you.<br /><br /></p>
                        <p>You can also download the Slack on your Android or IOS device. Click on the Google Play icon or the App store icon to be directed to the respective store.</p>', 2);       

insert into tbl_contentOther (content, page_id) values ('<p><a href="https://itunes.apple.com/nz/app/slack-business-communication-for-teams/id618783545?mt=8" target="_blank"><img src="../images/applestore.png" alt="" height="100" width="250"></a></p><p><a href="https://play.google.com/store/apps/details?id=com.Slack" target="_blank"><img src="../images/playstore.png" alt="" height="100" width="250"></a></p>', 2);









insert into tbl_contentOther (content, page_id) values ('<h1>What is Microsoft Imagine</h1>
                        <p>At the beginning of each semester we set the students up with a Microsoft Imagine Account.<br /><br />
                        This allows you to download some free products from Microsoft that you would otherwise have to pay for.<br /><br />
                        Once you have graduated or are no longer with us, you can keep the software and licenses, but you will not be able to access the website anymore.</p>', 3);
                                                                  

insert into tbl_contentOther (content, page_id) values ('<p><img src="../images/imagine.jpg" alt="" height="300" width="500"></p>', 3);

insert into tbl_contentOther (content, page_id) values ('<h1>How to set up your Microsoft Imagine Account</h1>
                        <p>In your bcs.net.nz email (it is a gmail account) you should have an email from Toi Ohomai and the subject should say:</p>
                        <ul>
                            <li><p>Your school created a Microsoft Imagine WebStore account for you</p></li>
                        </ul>
                        <p>Once you have graduated or are no longer with us, you can keep the software and licenses, but you will not be able to access the website anymore.</p>', 3);       

insert into tbl_contentOther (content, page_id) values ('<p><img src="../images/imagine-setup.gif" alt="" height="300" width="500"></p>', 3);









insert into tbl_contentOther (content, page_id) values ('<h1>Your files</h1>
                        <p>The corporate network (the library and classrooms other than the 3rd floor) is completely separate from the Pandora network (DT219, DT303, DT308 and DT312)<br /><br />
                        Both networks have an network drive for you to store you files in and both networks refer to that drive as H-Drive, however they are different and do not relate to each other in anyway.<br /><br />
                        By using your Google Apps account you are able to access your files from any machine using a web browser and you will not be limited by this.</p>', 4);
                                                                  

insert into tbl_contentOther (content, page_id) values ('<h1>Can I access my Pandora files at home?</h1>
                        <p>You are not able to access anything that you can stored on the pandora network.<br /><br />
                        However, you will be able to access course material that teachers have put on Moodle. You can also upload your files to Google drive and access them later from any location.<br /><br />
                        The software that is required for class work will always be available to you either on Microsoft Imagine or through a student service provided to you. The majority of the software is free of charge for everyone.<br/><br/></p> ', 4);

insert into tbl_contentOther (content, page_id) values ('<h1>Toi Ohomai WiFi Network</h1>
                        <p>The student Wi-Fi is also completely separate from the Pandora Network. If you are using your own device or connect your mobile device, the easiest way to access your files is by using Google Apps<br /><br />
                        The Wifi network basically allows you to browse the web. There are some exceptions, but you will hear about that in the Web Development papers you will do in your degree.</p>', 4);       

insert into tbl_contentOther (content, page_id) values ('<h1>Any further questions?</h1>
                        <p>If you have any questions, ask any of the IT staff and we can help you out.<br /><br />
                        You can also ask your questions on Slack in the <a class="helpful" href="https://to-bcs.slack.com/messages/general" target="_blank">#general channel</a>, maybe other students can help you out as well!</p>', 4);

DROP TABLE IF EXISTS `tbl_footer`;
CREATE TABLE `tbl_footer` (
  `ID` int(11) NOT NULL,
  `CONTENT` varchar(256) NOT NULL,
  PRIMARY KEY (`ID`)
);

insert into tbl_footer (id, content) values (1, '<p class="footerleft">&copy; Copyright Toi Ohomai 2016 - All rights reserved</p>
                <a href="https://toiohomai.ac.nz/contact-us" class="footerright" target="_blank">Contact Us</a>');