<?php   include_once('../functions/functions.php');
        include_once('../functions/functionsnav.php');
        include_once('../functions/functionsjumbo.php');
        include_once('../functions/functionsquicklink.php');
        include_once('../functions/functionscontentindex.php');
        include_once('../functions/functionsfooter.php');
session_start();
loginAdmin(); ?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>BCS - Pandora</title>
        <link rel="icon" href="https://toiohomai.ac.nz/sites/default/files/favicon_0.ico">

        <!-- Bootstrap stylesheets -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"/>

        <!-- Custom stylesheet -->
        <link rel="stylesheet" href="../css/styles.css" type="text/css" >

    </head>
    <body>
        <div class="navbar-custom">
            <div class="container-fluid">
                <nav class="navbar navbar-static-top">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="https://toiohomai.ac.nz/">
                            <img src="../images/toi_logo.png" alt="Toi-Ohomai"></a>
                        </div>
                        <div id="navbar" class="navbar-collapse collapse">
                            <ul class="nav navbar-nav">
                                <li class="down"><a href="index.php" class="">Home</a> </li>
                                <?php echo show_contentNav(get_all_Nav(), "index"); ?>
                                <!--<li class="down"><a href="googleapps.php" class="">Google Apps</a><a href="editnav.php"><p class="editnav">edit</p></a></li>
                                <li class="down"><a href="slack.php" class="">Slack</a><a href="editnav.php?id="><p class="editnav">edit</p></a></li>
                                <li class="down"><a href="imagine.php" class="">Microsoft Imagine</a><a href="editnav.php"><p class="editnav">edit</p></a></li>
                                <li class="down"><a href="pandora.php" class="">Pandora</a><a href="editnav.php"><p class="editnav">edit</p></a></li>-->
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </div>

    <!-- Page Content -->
    <div class="container maincontent">
        <!-- Content beings here -->        
        <?php 
        if(!@$_SESSION['login']) 
        {
        ?>
        <div class = "container">
	        <div class="wrapper">
                <form action="" method="post" name="Login_Form" class="form-signin">
                    <h3 class="form-signin-heading">Admin Login<br><br>Please ensure you logout after use.</h3>
                    <hr class="colorgraph"><br>
                    
                    <input type="text" class="form-control" name="Username" placeholder="Username" required="" autofocus="" />
                    <input type="password" class="form-control" name="Password" placeholder="Password" required=""/>     		  
                    
                    <button class="btn btn-lg btn-primary btn-block"  name="login" value="Login" type="Submit">Login</button>  			
                </form>			
	        </div>
        </div>


        <?php
        }
        else
        {
            redirect("../admin/index.php");
        }
        ?>
            <div class="footer">
                <hr class="footerline">
                <?php echo show_Footer(get_all_Footer(), "index"); ?>
            </div>
        </div>

    <!-- Scripts -->
    <script type="text/javascript" src="javascript/script.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

    </body>
</html>