<?php   include_once('../functions/functions.php');
        include_once('../functions/functionsnav.php');
        include_once('../functions/functionsjumbo.php');
        include_once('../functions/functionsquicklink.php');
        include_once('../functions/functionscontentindex.php');
        include_once('../functions/functionsfooter.php');
        session_start();
?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>BCS - Home</title>
        <link rel="icon" href="https://toiohomai.ac.nz/sites/default/files/favicon_0.ico">

        <!-- Bootstrap stylesheets -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom stylesheet -->
        <link rel="stylesheet" href="../css/styles.css" type="text/css">
        
    </head>
    <body>
        <div class="navbar-custom">
            <div class="container-fluid">
                <nav class="navbar navbar-static-top">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="https://toiohomai.ac.nz/">
                            <img src="../images/toi_logo.png" alt="Toi-Ohomai"></a>
                        </div>
                        <div id="navbar" class="navbar-collapse collapse">
                            <ul class="nav navbar-nav">
                                <li class="down"><a href="index.php" class="">Home</a></li>
                                <?php echo show_contentNav(get_all_Nav(), "index"); ?>
                            </ul>
                            <ul class="nav navbar-nav pull-right">
                                <li class="down"><a href="login.php">Login</a></li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </div>

        <div class="jumbotron">
            <div class="container">
                <h1 class="display-3"><div class="heading">Welcome to Pandora</div></h1>
                <p><div class="sub-heading">This site will give you an introduction into what the Pandora network offers</div></p>
                <p><div class="header-button"><a class="btn btn-primary btn-lg" href="https://toiohomai.ac.nz/" target="_blank" role="button">Toi Ohomai Site &raquo;</a></div></p>
            </div>
        </div>

        <div class="container maincontent">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Quick links</h1>
                </div>
                <?php echo show_contentQuicklink(get_all_quicklink(), "index"); ?>
            </div>
            <div class="row">
                <div class="info">
                <?php echo show_contentContent(get_all_content(), "index"); ?>

                </div>
            </div>
            <div>
                <hr>
                <?php echo show_Footer(get_all_Footer(), "index"); ?>
            </div>
        </div>


    <!-- Scripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="javascript/script.js"></script>

    </body>
</html>