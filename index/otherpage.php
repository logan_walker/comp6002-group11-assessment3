<?php   include_once('../functions/functions.php');
        include_once('../functions/functionsnav.php');
        include_once('../functions/functionsjumbo.php');
        include_once('../functions/functionsquicklink.php');
        include_once('../functions/functionsotherpage.php');
        include_once('../functions/functionsfooter.php');
        session_start();

        $id = $_GET['id'];
?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>BCS - Home</title>
        <link rel="icon" href="https://toiohomai.ac.nz/sites/default/files/favicon_0.ico">

        <!-- Bootstrap stylesheets -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom stylesheet -->
        <link rel="stylesheet" href="../css/styles.css" type="text/css">
        
    </head>
    <body>
        <div class="navbar-custom">
            <div class="container-fluid">
                <nav class="navbar navbar-static-top">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="https://toiohomai.ac.nz/">
                            <img src="../images/toi_logo.png" alt="Toi-Ohomai"></a>
                        </div>
                        <div id="navbar" class="navbar-collapse collapse">
                            <ul class="nav navbar-nav">
                                <li class="down"><a href="index.php" class="">Home</a></li>
                                <?php echo show_contentNav(get_all_Nav(), "index"); ?>
                            </ul>
                            <ul class="nav navbar-nav pull-right">
                                <li class="down"><a href="login.php">Login</a></li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </div>

        <?php echo show_contentJumbo_other(get_all_Jumbo_other($id), "index"); ?>

        <div class="container maincontent">
        <div class="row">
            <?php echo show_contentContentOther(get_all_contentOther($id), "index"); ?>
        </div>
        <div>
            <hr>
            <?php echo show_Footer(get_all_Footer(), "index"); ?>
        </div>
    </div>
    

    <!-- Scripts -->
    <script type="text/javascript" src="javascript/script.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

    </body>
</html>