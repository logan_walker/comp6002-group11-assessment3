<?php

    include_once('creds.php');
    
/////////////////////////////////////GET ITEMS///////////////////////////////////////////////////

    function get_all_quicklink() {
        
        $db = connection();
        $sql = "SELECT * FROM tbl_quicklink";
        $arr = [];

        $result = $db->query($sql);
    
        if(!$result) {
            die("There was an error running the query [".$db->error."] ");
        }

        while ($row = $result->fetch_assoc()) {
            $arr[] = array (
                "id" => $row['ID'],
                "imgsrc" => $row['IMGSRC'],
                "link" => $row['LINK'],
                "alt" => $row['ALT']
            );
        }

        $json = json_encode($arr);
    
        $result->free();
        $db->close();
        
        return $json;
    }

//////////////////////////////////////////SHOW DATA//////////////////////////////////////////////////////////////////////////////    

    function show_contentQuicklink($data, $page) {
    
        $array = json_decode($data, True);
        
        $output = "";

        if (count($array) > 0 ) {
            for ($i = 0; $i < count($array); $i++) {
                
                if ($page == "index") {
                    $output .= "<div class=\"col-lg-3 col-md-4 col-xs-6 thumb\">
                                    <a class=\"thumbnail\" href=\"".$array[$i]['link']."\" target=\"_blank\">
                                    <img src=\"../".$array[$i]['imgsrc']."\" href=\"".$array[$i]['link']."\ alt=\"".$array[$i]['alt']."\">
                                    </a>
                                </div>";
                }
                
                if ($page == "admin") {
                    $output .= "<div class=\"col-lg-3 col-md-4 col-xs-6 thumb\">
                                    <a class=\"thumbnail\" href=\"".$array[$i]['link']."\" target=\"_blank\">
                                        <img src=\"../".$array[$i]['imgsrc']."\" href=\"".$array[$i]['link']."\ alt=\"".$array[$i]['alt']."\">
                                    </a>
                                    <p class=\"editquicklink\"><a class=\"btn btn-warning\" href=\"editquicklink.php?id=".$array[$i]['id']."\"\>Edit</a> - <a class=\"btn btn-danger\" href=\"deletequicklink.php?id=".$array[$i]['id']."\"\>Delete</a></p> 
                                </div>";
                }   
            }
            
            return $output;
        }
        
        else {
            $output .= "<tr><td colspan='5'>No Data Available</td></tr>";
            
            return $output;
        }
    }

////////////////////////////////////SAVING CHANGES///////////////////////////////////////////////////


    function editRecordQuickLink() {

        if(isset($_POST['updateQuickLink'])) {
            $db = connection();

            $imgsrc = $db->real_escape_string($_POST['imgsrc']);
            $link = $db->real_escape_string($_POST['link']);
            $alt = $db->real_escape_string($_POST['alt']);
            $id = $db->real_escape_string($_POST['id']);

            $sql = "UPDATE tbl_quicklink SET IMGSRC='".$imgsrc."', LINK='".$link."', ALT='".$alt."' WHERE ID = ".$id."";

            $result = $db->query($sql);

            if ($result == 1) {
                redirect("index.php");
            }
            else {
                print_r($sql);
                return "<br><br>An Error has occured";
                exit();
            }
        }  
    }

////////////////////////////////////DELETING RECORD//////////////////////////////////////////////////////

    function removeSingleRecordQuicklink() {
        if(isset($_POST['removeRecord'])) {
            $db = connection();

            $id = $db->real_escape_string($_POST['id']);

            $stmt = $db->prepare("DELETE FROM tbl_quicklink WHERE ID = $id");
            $stmt->bind_param("i", $id);
            $stmt->execute();
            
            print $stmt->error; //to check errors

            $result = $stmt->affected_rows;

            $stmt->close();
            $db->close();

            if ($result == 1) {
                redirect("index.php");
            }
            else {
                print_r($sql);
                echo "<br><br>";
                echo "An Error has occured";
            }
        }
    }


////////////////////////////////////ADDING RECORD//////////////////////////////////////////////////////

    function addRecordQuicklink(){
        if(isset($_POST['addLink'])) {
            $db = connection();

            $imgsrc = $db->real_escape_string($_POST['imgsrc']);
            $link = $db->real_escape_string($_POST['link']);
            $alt = $db->real_escape_string($_POST['alt']);

            $stmt = $db->prepare("INSERT INTO tbl_quicklink (IMGSRC, LINK, ALT) VALUES ('$imgsrc', '$link', '$alt')");
            $stmt->bind_param("sss", $imgsrc, $link, $alt);
            $stmt->execute();

            print $stmt->error; //to check errors

            $result = $stmt->affected_rows;

            $stmt->close();
            $db->close();

            if ($result > 0) {
                redirect("index.php");
            }
            else {
                //print_r($sql);
                echo "<br><br>";
                echo "An Error has occured";
            }
         }
    }


/////////////////////////////////////DISPLAY ITEMS IN EDIT///////////////////////////////////////////////////


    function displayIdQuickLink() {
        $id = $_GET['id'];
        $array = json_decode(loadQuicklinkData($id), True);
        return $array[0]['id'];
    }

    function displayImgQuickLink() {
        $id = $_GET['id'];
        $array = json_decode(loadQuicklinkData($id), True);
        return $array[0]['imgsrc'];
    }

    function displayLinkQuickLink() {
        $id = $_GET['id'];
        $array = json_decode(loadQuicklinkData($id), True);
        return $array[0]['link'];
    }

    function displayAltQuickLink() {
        $id = $_GET['id'];
        $array = json_decode(loadQuicklinkData($id), True);
        return $array[0]['alt'];
    }


//////////////////////////////////////////////LOAD DATA//////////////////////////////////////////////////////////////////////


    function loadQuicklinkData($id) {

        $db = connection();
        $sql = "SELECT * FROM tbl_quicklink WHERE ID = $id";
        $arr = [];
        
        $result = $db->query($sql);
        
        if(!$result) {
            die("There was an error running the query [".$db->error."] ");
        }
        
        while ($row = $result->fetch_assoc()) {
            $arr[] = array (
                "id" => $row['ID'],
                "imgsrc" => $row['IMGSRC'],
                "link" => $row['LINK'],
                "alt" => $row['ALT']
            );
        }
        
        $json = json_encode($arr);
        
        $result->free();
        $db->close();
        
        return $json;        
    }