<?php

    include_once('creds.php');

    /////////////////////////////////////GET ITEMS///////////////////////////////////////////////////

    function get_all_Jumbo() {
        
        $db = connection();
        $sql = "SELECT * FROM tbl_jumbo";
        $arr = [];

        $result = $db->query($sql);
    
        if(!$result) {
            die("There was an error running the query [".$db->error."] ");
        }

        while ($row = $result->fetch_assoc()) {
            $arr[] = array (
                "id" => $row['ID'],
                "title" => $row['TITLE'],
                "subtitle" => $row['SUBTITLE'],
                "buttontitle" => $row['BUTTONTITLE'],
                "buttonlink" => $row['BUTTONLINK']
            );
        }

        $json = json_encode($arr);
    
        $result->free();
        $db->close();
        
        return $json;
    }

    //////////////////////////////////////////SHOW DATA//////////////////////////////////////////////////////////////////////////////

    function show_contentJumbo($data, $page) {
    
        $array = json_decode($data, True);
        
        $output = "";

        if (count($array) > 0 ) {
            for ($i = 0; $i < count($array); $i++) {
                if ($page == "index") {
                    $output .= "<h1 class=\"display-3\"><div class=\"heading\">".$array[$i]['title']." <p class=\"editnav\">edit</p> </div></h1>
                                <p><div class=\"sub-heading\">".$array[$i]['subtitle']."</div></p>
                                <p><div class=\"header-button\"><a class=\"btn btn-primary btn-lg\" href=\"".$array[$i]['buttonlink']."\" target=\"_blank\" role=\"button\">".$array[$i]['buttontitle']." &raquo;</a></div></p>";
                }
                
                if ($page == "admin") {
                    //String for HTML table code
                    //<td>".$array[$i]['category']."</td>
                    $output .= "<h1 class=\"display-3\"><div class=\"heading\">".$array[$i]['title']."<a href=\"newpage.php\"><p class=\"btn btn-success\" class=\"editjumbo\" style=\"color:white; float:right\">Add New Page</p></a> </div></h1>
                                <p><div class=\"sub-heading\">".$array[$i]['subtitle']."</div></p>
                                <p><div class=\"header-button\"><a class=\"btn btn-primary btn-lg\" href=\"".$array[$i]['buttonlink']."\" target=\"_blank\" role=\"button\">".$array[$i]['buttontitle']." &raquo;</a><a href=\"editjumbo.php?id=".$array[$i]['id']."\"> <p class=\"btn btn-warning\" class=\"editjumbo\" style=\"color:white;float:right\">Edit Header</p></a></div></p>";

                //<h1 class="display-3"><div class="heading">Welcome to Pandora</div></h1>
                //<p><div class="sub-heading">This site will give you an introduction into what the Pandora network offers</div></p>
                //<p><div class="header-button"><a class="btn btn-primary btn-lg" href="https://toiohomai.ac.nz/" target="_blank" role="button">Toi Ohomai Site &raquo;</a></div></p>
                } 
            }
            return $output;
        }
        
        else {
            $output .= "<tr><td colspan='5'>No Data Available</td></tr>";
            
            return $output;
        }
    }

////////////////////////////////////SAVING CHANGES///////////////////////////////////////////////////

    function editRecordJumbo() {

        if(isset($_POST['updateJumbo'])) {
            $db = connection();

            $title = $db->real_escape_string($_POST['title']);
            $subtitle = $db->real_escape_string($_POST['subtitle']);
            $buttontitle = $db->real_escape_string($_POST['buttontitle']);
            $buttonlink = $db->real_escape_string($_POST['buttonlink']);
            $id = $db->real_escape_string($_POST['id']);

            $sql = "UPDATE tbl_jumbo SET TITLE='".$title."', SUBTITLE='".$subtitle."', BUTTONTITLE='".$buttontitle."', BUTTONLINK='".$buttonlink."' WHERE ID = ".$id."";

            $result = $db->query($sql);

            if ($result == 1) {
                redirect("index.php");
            }
            else {
                print_r($sql);
                return "<br><br>An Error has occured";
                exit();
            }
        }  
    }

    ////////////////////////////////////DELETING RECORD//////////////////////////////////////////////////////

   /* function removeSingleRecordJumbo() {
        if(isset($_POST['removeRecord'])) {
            $db = connection();

            $id = $db->real_escape_string($_POST['id']);

            $stmt = $db->prepare("DELETE FROM tbl_jumbo WHERE ID = $id");
            $stmt->bind_param("i", $id);
            $stmt->execute();
            
            print $stmt->error; //to check errors

            $result = $stmt->affected_rows;

            $stmt->close();
            $db->close();

            if ($result == 1) {
                redirect("index.php");
            }
            else {
                print_r($sql);
                echo "<br><br>";
                echo "An Error has occured";
            }
        }
    }*/

    ////////////////////////////////////ADDING RECORD//////////////////////////////////////////////////////

  /*  function addRecordJumbo(){
        if(isset($_POST['addItem'])) {
            $db = connection();

            $title = $db->real_escape_string($_POST['title']);
            $subtitle = $db->real_escape_string($_POST['subtitle']);
            $buttontitle = $db->real_escape_string($_POST['buttontitle']);
            $buttonlink = $db->real_escape_string($_POST['buttonlink']);

            $stmt = $db->prepare("INSERT INTO tbl_jumbo (TITLE, SUBTITLE, BUTTONTITLE, BUTTONLINK) VALUES ('$title', '$subtitle', '$buttontitle', '$buttonlink')");
            $stmt->bind_param("ssss", $title, $subtitle, $buttontitle, $buttonlink );
            $stmt->execute();

            print $stmt->error; //to check errors

            $result = $stmt->affected_rows;

            $stmt->close();
            $db->close();

            if ($result > 0) {
                redirect("index.php");
            }
            else {
                //print_r($sql);
                echo "<br><br>";
                echo "An Error has occured";
            }
         }
    }*/

        /////////////////////////////////////DISPLAY ITEMS IN EDIT///////////////////////////////////////////////////

    function displayIdJumbo() {
        $id = $_GET['id'];
        $array = json_decode(loadJumboData($id), True);
        return $array[0]['id'];
    }

    function displayTitleJumbo() {
        $id = $_GET['id'];
        $array = json_decode(loadJumboData($id), True);
        return $array[0]['title'];
    }

    function displaySubtitleJumbo() {
        $id = $_GET['id'];
        $array = json_decode(loadJumboData($id), True);
        return $array[0]['subtitle'];
    }

    function displayButtonTitleJumbo() {
        $id = $_GET['id'];
        $array = json_decode(loadJumboData($id), True);
        return $array[0]['buttontitle'];
    }

    function displayButtonLinkJumbo() {
        $id = $_GET['id'];
        $array = json_decode(loadJumboData($id), True);
        return $array[0]['buttonlink'];
    }

    //////////////////////////////////////////////LOAD DATA//////////////////////////////////////////////////////////////////////

    function loadJumboData($id) {

        $db = connection();
        $sql = "SELECT * FROM tbl_jumbo_index WHERE ID = $id";
        $arr = [];
        
        $result = $db->query($sql);
        
        if(!$result) {
            die("There was an error running the query [".$db->error."] ");
        }
        
        while ($row = $result->fetch_assoc()) {
            $arr[] = array (
                "id" => $row['ID'],
                "title" => $row['TITLE'],
                "subtitle" => $row['SUBTITLE'],
                "buttontitle" => $row['BUTTONTITLE'],
                "buttonlink" => $row['BUTTONLINK']
            );
        }
        
        $json = json_encode($arr);
        
        $result->free();
        $db->close();
        
        return $json;        
    }

    /////////////////////////////////////GET ITEMS///////////////////////////////////////////////////

    function get_all_Jumbo_Index() {
        
        $db = connection();
        $sql = "SELECT * FROM tbl_jumbo_Index";
        $arr = [];

        $result = $db->query($sql);
    
        if(!$result) {
            die("There was an error running the query [".$db->error."] ");
        }

        while ($row = $result->fetch_assoc()) {
            $arr[] = array (
                "id" => $row['ID'],
                "title" => $row['TITLE'],
                "subtitle" => $row['SUBTITLE'],
                "buttontitle" => $row['BUTTONTITLE'],
                "buttonlink" => $row['BUTTONLINK']
            );
        }

        $json = json_encode($arr);
    
        $result->free();
        $db->close();
        
        return $json;
    }

    //////////////////////////////////////////SHOW DATA//////////////////////////////////////////////////////////////////////////////

    function show_contentJumbo_Index($data, $page) {
    
        $array = json_decode($data, True);
        
        $output = "";

        if (count($array) > 0 ) {
            for ($i = 0; $i < count($array); $i++) {
                
                if ($page == "index") {
                    $output .= "<h1 class=\"display-3\"><div class=\"heading\">".$array[$i]['title']." <p class=\"editnav\">edit</p> </div></h1>
                                <p><div class=\"sub-heading\">".$array[$i]['subtitle']."</div></p>
                                <p><div class=\"header-button\"><a class=\"btn btn-primary btn-lg\" href=\"".$array[$i]['buttonlink']."\" target=\"_blank\" role=\"button\">".$array[$i]['buttontitle']." &raquo;</a></div></p>";
                }
                
                if ($page == "admin") {
                    //String for HTML table code
                    //<td>".$array[$i]['category']."</td>
                    $output .= "<h1 class=\"display-3\"><div class=\"heading\">".$array[$i]['title']."<a href=\"editjumbo.php?id=".$array[$i]['id']."\"> <p class=\"editjumbo\">Edit Header</p></a> </div></h1>
                                <p><div class=\"sub-heading\">".$array[$i]['subtitle']."</div></p>
                                <p><div class=\"header-button\"><a class=\"btn btn-primary btn-lg\" href=\"".$array[$i]['buttonlink']."\" target=\"_blank\" role=\"button\">".$array[$i]['buttontitle']." &raquo;</a></div></p>";

                //<h1 class="display-3"><div class="heading">Welcome to Pandora</div></h1>
                //<p><div class="sub-heading">This site will give you an introduction into what the Pandora network offers</div></p>
                //<p><div class="header-button"><a class="btn btn-primary btn-lg" href="https://toiohomai.ac.nz/" target="_blank" role="button">Toi Ohomai Site &raquo;</a></div></p> class=\"btn btn-danger\"
                }   
            }
            
            return $output;
        }
        
        else {
            $output .= "<tr><td colspan='5'>No Data Available</td></tr>";
            
            return $output;
        }
    }

////////////////////////////////////SAVING CHANGES///////////////////////////////////////////////////

    function editRecordJumbo_Index() {

        if(isset($_POST['updateJumbo_Index'])) {
            $db = connection();

            $title = $db->real_escape_string($_POST['title']);
            $subtitle = $db->real_escape_string($_POST['subtitle']);
            $buttontitle = $db->real_escape_string($_POST['buttontitle']);
            $buttonlink = $db->real_escape_string($_POST['buttonlink']);
            $id = $db->real_escape_string($_POST['id']);

            $sql = "UPDATE tbl_jumbo_index SET TITLE='".$title."', SUBTITLE='".$subtitle."', BUTTONTITLE='".$buttontitle."', BUTTONLINK='".$buttonlink."' WHERE ID = ".$id."";

            $result = $db->query($sql);

            if ($result == 1) {
                redirect("index.php");
            }
            else {
                print_r($sql);
                return "<br><br>An Error has occured";
                exit();
            }
        }  
    }

    ////////////////////////////////////DELETING RECORD//////////////////////////////////////////////////////

    function removeSingleRecordJumbo_Index() {
        if(isset($_POST['removeRecord_Index'])) {
            $db = connection();

            $id = $db->real_escape_string($_POST['id']);

            $stmt = $db->prepare("DELETE FROM tbl_jumbo_Index WHERE ID = $id");
            $stmt->bind_param("i", $id);
            $stmt->execute();
            
            print $stmt->error; //to check errors

            $result = $stmt->affected_rows;

            $stmt->close();
            $db->close();

            if ($result == 1) {
                redirect("index.php");
            }
            else {
                print_r($sql);
                echo "<br><br>";
                echo "An Error has occured";
            }
        }
    }

    ////////////////////////////////////ADDING RECORD//////////////////////////////////////////////////////

    function addRecordJumbo_Index(){
        if(isset($_POST['addItem'])) {
            $db = connection();

            $title = $db->real_escape_string($_POST['title']);
            $subtitle = $db->real_escape_string($_POST['subtitle']);
            $buttontitle = $db->real_escape_string($_POST['buttontitle']);
            $buttonlink = $db->real_escape_string($_POST['buttonlink']);

            $stmt = $db->prepare("INSERT INTO tbl_jumbo_Index (TITLE, SUBTITLE, BUTTONTITLE, BUTTONLINK) VALUES ('$title', '$subtitle', '$buttontitle', '$buttonlink')");
            $stmt->bind_param("ssss", $title, $subtitle, $buttontitle, $buttonlink );
            $stmt->execute();

            print $stmt->error; //to check errors

            $result = $stmt->affected_rows;

            $stmt->close();
            $db->close();

            if ($result > 0) {
                redirect("index.php");
            }
            else {
                //print_r($sql);
                echo "<br><br>";
                echo "An Error has occured";
            }
         }
    }

        /////////////////////////////////////DISPLAY ITEMS IN EDIT///////////////////////////////////////////////////

    function displayIdJumbo_Index() {
        $id = $_GET['id'];
        $array = json_decode(loadJumboData_Index($id), True);
        return $array[0]['id'];
    }

    function displayTitleJumbo_Index() {
        $id = $_GET['id'];
        $array = json_decode(loadJumboData_Index($id), True);
        return $array[0]['title'];
    }

    function displaySubtitleJumbo_Index() {
        $id = $_GET['id'];
        $array = json_decode(loadJumboData_Index($id), True);
        return $array[0]['subtitle'];
    }

    function displayButtonTitleJumbo_Index() {
        $id = $_GET['id'];
        $array = json_decode(loadJumboData_Index($id), True);
        return $array[0]['buttontitle'];
    }

    function displayButtonLinkJumbo_Index() {
        $id = $_GET['id'];
        $array = json_decode(loadJumboData_Index($id), True);
        return $array[0]['buttonlink'];
    }

    //////////////////////////////////////////////LOAD DATA//////////////////////////////////////////////////////////////////////

    function loadJumboData_Index($id) {

        $db = connection();
        $sql = "SELECT * FROM tbl_jumbo_index WHERE ID = $id";
        $arr = [];
        
        $result = $db->query($sql);
        
        if(!$result) {
            die("There was an error running the query [".$db->error."] ");
        }
        
        while ($row = $result->fetch_assoc()) {
            $arr[] = array (
                "id" => $row['ID'],
                "title" => $row['TITLE'],
                "subtitle" => $row['SUBTITLE'],
                "buttontitle" => $row['BUTTONTITLE'],
                "buttonlink" => $row['BUTTONLINK']
            );
        }
        
        $json = json_encode($arr);
        
        $result->free();
        $db->close();
        
        return $json;        
    }

?>