<?php
    
    include_once('creds.php');

    /////////////////////////////////////GET ITEMS///////////////////////////////////////////////////

    function get_all_contentOther($id) {
        
        $db = connection();
        $sql = "SELECT * FROM tbl_contentother WHERE page_id = $id";
        $arr = [];

        $result = $db->query($sql);
    
        if(!$result) {
            die("There was an error running the query [".$db->error."] ");
        }

        while ($row = $result->fetch_assoc()) {
            $arr[] = array (
                "id" => $row['ID'],
                "page_id" => $row['PAGE_ID'],
                "content" => $row['CONTENT']
            );
        }

        $json = json_encode($arr);
    
        $result->free();
        $db->close();
        
        return $json;
    }

    function get_all_Jumbo_other($id) {
        
        $db = connection();
        $sql = "SELECT * FROM tbl_jumbo WHERE page_id = $id";
        $arr = [];

        $result = $db->query($sql);
    
        if(!$result) {
            die("There was an error running the query [".$db->error."] ");
        }

        while ($row = $result->fetch_assoc()) {
            $arr[] = array (
                "page_id" => $row['PAGE_ID'],
                "title" => $row['TITLE'],
                "subtitle" => $row['SUBTITLE'],
                "buttontitle" => $row['BUTTONTITLE'],
                "buttonlink" => $row['BUTTONLINK']
            );
        }

        $json = json_encode($arr);
    
        $result->free();
        $db->close();
        
        return $json;
    }


//////////////////////////////////////////SHOW DATA//////////////////////////////////////////////////////////////////////////////

    function show_contentContentOther($data, $page) {
    
        $array = json_decode($data, True);
        
        $output = "";

        if (count($array) > 0 ) {
            for ($i = 0; $i < count($array); $i++) {
                
                if ($page == "index") {
                    $output .= "<div class=\"col-xs-12 col-md-6\">".$array[$i]['content']."</div>";
                }
                
                if ($page == "admin") {
                    //String for HTML table code
                    //<td>".$array[$i]['category']."</td>
                    $output .= "<div tag=\"emptyspace\" class=\"col-xs-12 col-md-6\">
                                    <p class=\"editquicklink\"><a class=\"btn btn-warning\" href=\"editcontentother.php?id=".$array[$i]['id']."\"\> Edit </a> - <a class=\"btn btn-danger\" href=\"deletecontentother.php?id=".$array[$i]['id']."\"\>Delete</a></p>
                                    ".$array[$i]['content']."
                                </div>";  
                    //add button moved but code is here so can be moved back
                    //<a href=\"addnav.php?id=".$array[$i]['id']."\"\> <p class=\"editnav\">add</p> </a>
                }   
            }
            
            return $output;
        }
        
        else {
            //$output .= "<tr><td colspan='5'>No Data Available</td></tr>";
            $output .= "";
            
            return $output;
        }
    }

    function show_contentJumbo_other($data, $page) {
    
        $array = json_decode($data, True);
        
        $output = "";

        if (count($array) > 0 ) {
            for ($i = 0; $i < count($array); $i++) {
                
                    

                if ($page == "index") {
                    $output .= "<div class=\"jumbotron\">
                                    <div class=\"container\">
                                        <h1 class=\"display-3\"><div class=\"heading\">".$array[$i]['title']." </div></h1>
                                        <p><div class=\"sub-heading\">".$array[$i]['subtitle']."</div></p>
                                        <p><div class=\"header-button\"><a class=\"btn btn-primary btn-lg\" href=\"".$array[$i]['buttonlink']."\" target=\"_blank\" role=\"button\">".$array[$i]['buttontitle']." &raquo;</a></div></p>
                                    </div>
                                </div>";
                }
                
                if ($page == "admin") {
                    //String for HTML table code
                    //<td>".$array[$i]['category']."</td>
                    $output .= "<div class=\"jumbotron\">
                                    <div class=\"container\">
                                        <h1 class=\"display-3\"><div class=\"heading\">".$array[$i]['title']."<a href=\"newpage.php\"><p class=\"btn btn-success\" class=\"editjumbo\" style=\"color:white; float:right\">Add New Page</p></a></div></h1>
                                        <p><div class=\"sub-heading\">".$array[$i]['subtitle']."</div></p>
                                        <p><div class=\"header-button\"><a class=\"btn btn-primary btn-lg\" href=\"".$array[$i]['buttonlink']."\" target=\"_blank\" role=\"button\">".$array[$i]['buttontitle']." &raquo;</a><a href=\"editjumboother.php?id=".$array[$i]['page_id']."\"><p class=\"btn btn-warning\" class=\"editjumbo\" style=\"color:white; float:right\">Edit Header</p></a></div></p>
                                    </div>
                                </div>";

                //<h1 class="display-3"><div class="heading">Welcome to Pandora</div></h1>
                //<p><div class="sub-heading">This site will give you an introduction into what the Pandora network offers</div></p>    <button type=\"button\" class=\"btn btn-danger\">ddd</button> style=\"color: white;\"
                //<p><div class="header-button"><a class="btn btn-primary btn-lg" href="https://toiohomai.ac.nz/" target="_blank" role="button">Toi Ohomai Site &raquo;</a></div></p>   style=\"color:white;float:right\">
                }   
            }
            
            return $output;
        }
        
        else {
            $output .= "<tr><td colspan='5'>No Data Available</td></tr>";
            
            return $output;
        }
    }

////////////////////////////////////SAVING CHANGES///////////////////////////////////////////////////

    function editRecordContentOther() {

        if(isset($_POST['updateContent'])) {
            $db = connection();

            $content = $db->real_escape_string($_POST['content']);
            $id = $db->real_escape_string($_POST['id']);

            $sql = "UPDATE tbl_contentother SET CONTENT='".$content."' WHERE ID = ".$id."";

            $result = $db->query($sql);

            if ($result == 1) {
                redirect("otherpage.php?id=".$id."");
            }
            else {
                print_r($sql);
                return "<br><br>An Error has occured";
                exit();
            }
        }  
    }

    function editRecordJumboOther() {

        if(isset($_POST['updateJumbo'])) {
            $db = connection();

            $title = $db->real_escape_string($_POST['title']);
            $subtitle = $db->real_escape_string($_POST['subtitle']);
            $buttontitle = $db->real_escape_string($_POST['buttontitle']);
            $buttonlink = $db->real_escape_string($_POST['buttonlink']);
            $id = $db->real_escape_string($_POST['id']);

            $sql = "UPDATE tbl_jumbo SET TITLE='".$title."', SUBTITLE='".$subtitle."', BUTTONTITLE='".$buttontitle."', BUTTONLINK='".$buttonlink."' WHERE PAGE_ID = ".$id."";

            $result = $db->query($sql);

            if ($result == 1) {
                redirect("otherpage.php?id=".$id."");
            }
            else {
                print_r($sql);
                return "<br><br>An Error has occured";
                exit();
            }
        }  
    }

////////////////////////////////////DELETING RECORD//////////////////////////////////////////////////////

    function removeSingleRecordContentOther() {
        if(isset($_POST['removeRecord'])) {
            $db = connection();

            $id = $db->real_escape_string($_POST['id']);

            $stmt = $db->prepare("DELETE FROM tbl_contentother WHERE ID = $id");
            $stmt->bind_param("i", $id);
            $stmt->execute();
            
            print $stmt->error; //to check errors

            $result = $stmt->affected_rows;

            $stmt->close();
            $db->close();

            if ($result == 1) {
                redirect("otherpage.php?id=".$id."");
            }
            else {
                print_r($sql);
                echo "<br><br>";
                echo "An Error has occured";
            }
        }
    }

    function removePage() {
        if(isset($_POST['removePage'])) {
            /*$db = connection();

            $id = $db->real_escape_string($_POST['id']);

            $stmt = $db->prepare("DELETE FROM tbl_contentother WHERE ID = $id");
            $stmt->bind_param("i", $id);
            $stmt->execute();
            
            print $stmt->error; //to check errors

            $result = $stmt->affected_rows;

            $stmt->close();
            $db->close();*/

            $db = connection();

            $id = $db->real_escape_string($_POST['id']);

            $stmt = $db->prepare("DELETE FROM tbl_pages WHERE PAGE_ID = $id");
            $stmt->execute();
            
            print $stmt->error; //to check errors
            $result = $stmt->affected_rows;
            $stmt->close();

            $stmt = $db->prepare("DELETE FROM tbl_nav WHERE PAGE_ID = $id");
            $stmt->execute();

            print $stmt->error; //to check errors
            $stmt->close();

            $stmt = $db->prepare("DELETE FROM tbl_jumbo WHERE PAGE_ID = $id");
            $stmt->execute();

            print $stmt->error; //to check errors
            $stmt->close();

            $stmt = $db->prepare("DELETE FROM tbl_contentother WHERE PAGE_ID = $id");
            $stmt->execute();

            print $stmt->error; //to check errors
            $stmt->close();

            $db->close();

            if ($result == 1) {
                redirect("index.php");
            }
            else {
                echo "<br><br>";
                echo "An Error has occured";
            }
        }
    }

////////////////////////////////////ADDING RECORD//////////////////////////////////////////////////////

    function addRecordContentOther(){
        if(isset($_POST['addItem'])) {
            $db = connection();

            $content = $db->real_escape_string($_POST['content']);
            $id = $db->real_escape_string($_POST['id']);

            $stmt = $db->prepare("INSERT INTO tbl_contentother (CONTENT, PAGE_ID) VALUES ('$content', '$id')");
            $stmt->bind_param("si", $content, $id);
            $stmt->execute();

            print $stmt->error; //to check errors

            $result = $stmt->affected_rows;

            $stmt->close();
            $db->close();

            if ($result > 0) {
                redirect("otherpage.php?id=".$id."");
            }
            else {
                //print_r($sql);
                echo "<br><br>";
                echo "An Error has occured";
            }
         }
    }

    /////////////////////////////////////DISPLAY ITEMS IN EDIT///////////////////////////////////////////////////

    function displayIdContentOther() {
        $id = $_GET['id'];
        $array = json_decode(loadContentContentOther($id), True);
        return $array[0]['id'];
    }

    function displayContentContentOther() {
        $id = $_GET['id'];
        $array = json_decode(loadContentContentOther($id), True);
        return $array[0]['content'];
    }

////////////////////////////////////////////////////////////

    function displayIdJumbo_other() {
        $id = $_GET['id'];
        $array = json_decode(loadJumboData_other($id), True);
        return $array[0]['page_id'];
    }

    function displayTitleJumbo_other() {
        $id = $_GET['id'];
        $array = json_decode(loadJumboData_other($id), True);
        return $array[0]['title'];
    }

    function displaySubtitleJumbo_other() {
        $id = $_GET['id'];
        $array = json_decode(loadJumboData_other($id), True);
        return $array[0]['subtitle'];
    }

    function displayButtonTitleJumbo_other() {
        $id = $_GET['id'];
        $array = json_decode(loadJumboData_other($id), True);
        return $array[0]['buttontitle'];
    }

    function displayButtonLinkJumbo_other() {
        $id = $_GET['id'];
        $array = json_decode(loadJumboData_other($id), True);
        return $array[0]['buttonlink'];
    }


//////////////////////////////////////////////LOAD DATA//////////////////////////////////////////////////////////////////////

    function loadContentContentOther($id) {

        $db = connection();
        $sql = "SELECT * FROM tbl_contentother WHERE ID = $id";
        $arr = [];
        
        $result = $db->query($sql);
        
        if(!$result) {
            die("There was an error running the query [".$db->error."] ");
        }
        
        while ($row = $result->fetch_assoc()) {
            $arr[] = array (
                "id" => $row['ID'],
                "content" => $row['CONTENT']
            );
        }
        
        $json = json_encode($arr);
        
        $result->free();
        $db->close();
        
        return $json;        
    }

    function loadJumboData_other($id) {

        $db = connection();
        $sql = "SELECT * FROM tbl_jumbo WHERE PAGE_ID = ".$id."";
        $arr = [];
        
        $result = $db->query($sql);
        
        if(!$result) {
            die("There was an error running the query [".$db->error."] ");
        }
        
        while ($row = $result->fetch_assoc()) {
            $arr[] = array (
                "title" => $row['TITLE'],
                "subtitle" => $row['SUBTITLE'],
                "buttontitle" => $row['BUTTONTITLE'],
                "buttonlink" => $row['BUTTONLINK'],
                "page_id" => $row['PAGE_ID'],
            );
        }
        
        $json = json_encode($arr);
        
        $result->free();
        $db->close();
        
        return $json;        
    }

?>