<?php
    
    include_once('creds.php');

    /////////////////////////////////////GET ITEMS///////////////////////////////////////////////////

    function get_all_nav() {
        
        $db = connection();
        $sql = "SELECT * FROM tbl_nav";
        $arr = [];

        $result = $db->query($sql);
    
        if(!$result) {
            die("There was an error running the query [".$db->error."] ");
        }

        while ($row = $result->fetch_assoc()) {
            $arr[] = array (
                "page_id" => $row['PAGE_ID'],
                "title" => $row['TITLE']
            );
        }

        $json = json_encode($arr);
    
        $result->free();
        $db->close();
        
        return $json;
    }

//////////////////////////////////////////SHOW DATA//////////////////////////////////////////////////////////////////////////////

    function show_contentNav($data, $page) {
    
        $array = json_decode($data, True);
        
        $output = "";

        if (count($array) > 0 ) {
            for ($i = 0; $i < count($array); $i++) {
                
                if ($page == "index") {
                    $output .= "<li class=\"down\">
                                    <a href=\"otherpage.php?id=".$array[$i]['page_id']."\" class=\"\">".$array[$i]['title']."</a>
                                </li>";
                }
                
                if ($page == "admin") {
                    //String for HTML table code
                    //<td>".$array[$i]['category']."</td>
                    $output .= "<li class=\"down\">
                                    <a href=\"otherpage.php?id=".$array[$i]['page_id']."\" class=\"\">".$array[$i]['title']."</a>
                                    <a href=\"editnav.php?id=".$array[$i]['page_id']."\"\><p class=\"btn btn-warning\" class=\"editnav\">Edit Nav</p></a>
                                    <a href=\"deletepage.php?id=".$array[$i]['page_id']."\"\><p class=\"btn btn-danger\" class=\"editnav\">Delete Page</p></a>
                                </li>";   
                    //add button moved but code is here so can be moved back
                    //<a href=\"addnav.php?id=".$array[$i]['id']."\"\> <p class=\"editnav\">add</p> </a>
                }   
            }
            
            return $output;
        }
        
        else {
            $output .= "<tr><td colspan='5'>No Data Available</td></tr>";
            
            return $output;
        }
    }

////////////////////////////////////SAVING CHANGES///////////////////////////////////////////////////

    function editRecordNav() {

        if(isset($_POST['updateNav'])) {
            $db = connection();

            $title = $db->real_escape_string($_POST['title']);
            $id = $db->real_escape_string($_POST['page_id']);

            $sql = "UPDATE tbl_nav SET TITLE='".$title."' WHERE PAGE_ID = ".$id."";

            $result = $db->query($sql);

            if ($result == 1) {
                redirect("index.php");
            }
            else {
                print_r($sql);
                return "<br><br>An Error has occured";
                exit();
            }
        }  
    }

////////////////////////////////////DELETING RECORD//////////////////////////////////////////////////////

    /*function removeSingleRecordNav() {
        if(isset($_POST['removeRecord'])) {
            $db = connection();

            $id = $db->real_escape_string($_POST['id']);

            $stmt = $db->prepare("DELETE FROM tbl_nav WHERE ID = $id");
            $stmt->bind_param("i", $id);
            $stmt->execute();
            
            print $stmt->error; //to check errors

            $result = $stmt->affected_rows;

            $stmt->close();
            $db->close();

            if ($result == 1) {
                redirect("index.php");
            }
            else {
                print_r($sql);
                echo "<br><br>";
                echo "An Error has occured";
            }
        }
    }*/

////////////////////////////////////ADDING RECORD//////////////////////////////////////////////////////

    function addRecordNav(){
        if(isset($_POST['addItem'])) {
            $db = connection();

            $title = $db->real_escape_string($_POST['title']);
            $link = $db->real_escape_string($_POST['link']);

            $stmt = $db->prepare("INSERT INTO tbl_nav (TITLE, LINK) VALUES ('$title', '$link')");
            $stmt->bind_param("ss", $title, $link);
            $stmt->execute();

            print $stmt->error; //to check errors

            $result = $stmt->affected_rows;

            $stmt->close();
            $db->close();

            if ($result > 0) {
                redirect("index.php");
            }
            else {
                //print_r($sql);
                echo "<br><br>";
                echo "An Error has occured";
            }
         }
    }

    /////////////////////////////////////DISPLAY ITEMS IN EDIT///////////////////////////////////////////////////

    function displayIdNav() {
        $id = $_GET['id'];
        $array = json_decode(loadNavData($id), True);
        return $array[0]['page_id'];
    }

    function displayNameNav() {
        $id = $_GET['id'];
        $array = json_decode(loadNavData($id), True);
        return $array[0]['title'];
    }

//////////////////////////////////////////////LOAD DATA//////////////////////////////////////////////////////////////////////

    function loadNavData($id) {

        $db = connection();
        $sql = "SELECT * FROM tbl_nav WHERE PAGE_ID = '$id'";
        $arr = [];
        
        $result = $db->query($sql);
        
        if(!$result) {
            die("There was an error running the query [".$db->error."] ");
        }
        
        while ($row = $result->fetch_assoc()) {
            $arr[] = array (
                "title" => $row['TITLE'],
                "page_id" => $row['PAGE_ID']
            );
        }
        
        $json = json_encode($arr);
        
        $result->free();
        $db->close();
        
        return $json;        
    }

?>