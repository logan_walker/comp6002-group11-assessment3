<?php
    
    include_once('creds.php');

    /////////////////////////////////////GET ITEMS///////////////////////////////////////////////////

    function get_all_Footer() {
        
        $db = connection();
        $sql = "SELECT * FROM tbl_footer";
        $arr = [];

        $result = $db->query($sql);
    
        if(!$result) {
            die("There was an error running the query [".$db->error."] ");
        }

        while ($row = $result->fetch_assoc()) {
            $arr[] = array (
                "id" => $row['ID'],
                "content" => $row['CONTENT']
            );
        }

        $json = json_encode($arr);
    
        $result->free();
        $db->close();
        
        return $json;
    }


//////////////////////////////////////////SHOW DATA//////////////////////////////////////////////////////////////////////////////

    function show_Footer($data, $page) {
    
        $array = json_decode($data, True);
        
        $output = "";

        if (count($array) > 0 ) {
            for ($i = 0; $i < count($array); $i++) {
                
                if ($page == "index") {
                    $output .= "".$array[$i]['content']."";
                }
                
                if ($page == "admin") {
                    //String for HTML table code
                    //<td>".$array[$i]['category']."</td>
                    $output .= "<p><a class=\"btn btn-warning\" href=\"editfooter.php?id=".$array[$i]['id']."\"> Edit Footer </a></p>
                    ".$array[$i]['content']."";
                    //add button moved but code is here so can be moved back
                    //<a href=\"addnav.php?id=".$array[$i]['id']."\"\> <p class=\"editnav\">add</p> </a>
                }   
            }
            
            return $output;
        }
        
        else {
            //$output .= "<tr><td colspan='5'>No Data Available</td></tr>";
            $output .= "";
            
            return $output;
        }
    }

    

////////////////////////////////////SAVING CHANGES///////////////////////////////////////////////////

    function editRecordFooter() {

        if(isset($_POST['updateFooter'])) {
            $db = connection();

            $content = $db->real_escape_string($_POST['content']);
            $id = $db->real_escape_string($_POST['id']);

            $sql = "UPDATE tbl_footer SET CONTENT='".$content."' WHERE ID = ".$id."";

            $result = $db->query($sql);

            if ($result == 1) {
                redirect("index.php");
            }
            else {
                print_r($sql);
                return "<br><br>An Error has occured";
                exit();
            }
        }  
    }

    /////////////////////////////////////DISPLAY ITEMS IN EDIT///////////////////////////////////////////////////

    function displayIdFooter() {
        $id = $_GET['id'];
        $array = json_decode(loadContentFooter($id), True);
        return $array[0]['id'];
    }

    function displayContentFooter() {
        $id = $_GET['id'];
        $array = json_decode(loadContentFooter($id), True);
        return $array[0]['content'];
    }


//////////////////////////////////////////////LOAD DATA//////////////////////////////////////////////////////////////////////

    function loadContentFooter($id) {

        $db = connection();
        $sql = "SELECT * FROM tbl_footer WHERE ID = $id";
        $arr = [];
        
        $result = $db->query($sql);
        
        if(!$result) {
            die("There was an error running the query [".$db->error."] ");
        }
        
        while ($row = $result->fetch_assoc()) {
            $arr[] = array (
                "id" => $row['ID'],
                "content" => $row['CONTENT']
            );
        }
        
        $json = json_encode($arr);
        
        $result->free();
        $db->close();
        
        return $json;        
    }

?>