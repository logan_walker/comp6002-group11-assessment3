<html>
<head>
    <script>
        function myFunction() {
            alert("Hello! I am an alert box!");
        }
    </script>
</head>
</html>

<?php
    
    include_once('creds.php');


    function connection() {
        $conn = new mysqli(DBHOST, DBUSER, DBPASS, DBNAME);
        
        if ($conn->connect_errno > 0) {
            die('Unable to connect to database ['.$conn->connect_errno.']');
        } return $conn;
    }


//////////////////////////////////////////////LOGIN/LOGOUT////////////////////////////////////////////////////////////////    

    function loginAdmin() {

        if(isset($_POST['login'])) {
            $db = connection();

            $user = $db->real_escape_string($_POST['Username']);
            $pass = $db->real_escape_string($_POST['Password']);

            $sql = "SELECT * FROM tbl_admin WHERE NAME = '$user' && PASSWRD = '$pass'";
            $arr = [];

            $result = $db->query($sql);

            if(!$result) {
                die("There was an error running the query [".$db->error."] ");
            }
            
            while ($row = $result->fetch_assoc()) {
                $arr[] = array (
                    "user" => $row['NAME'],
                    "pass" => $row['PASSWRD']
                );
            }

            $result->free();
            $db->close();

            if (($user == $arr[0]['user']) && ($pass == $arr[0]['pass'])) {
                $_SESSION['login'] = TRUE;
                redirect("../admin/index.php");
            }
            else {
                $_SESSION['login'] = FALSE;
                echo '<script language="javascript">';
                echo 'alert("Incorrect Username or Password")';
                echo '</script>';
                //echo "<h1 class='removeSure'>Your login details are incorrect.</h1>";
            }
        }
    }

    function logout($logout = false) {
        if(@$logout) { // CHECK IF ACTUALLY WANT TO LOGOUT
            session_start();

            // Unset all of the session variables.
            $_SESSION = array();

            // If it's desired to kill the session, also delete the session cookie.
            // Note: This will destroy the session, and not just the session data!

            if (ini_get("session.use_cookies")) {
                $params = session_get_cookie_params();
                setcookie(session_name(), '', time() - 42000,
                    $params["path"], $params["domain"],
                    $params["secure"], $params["httponly"]
                );
            }

            // Finally, destroy the session.
            session_destroy();
            //redirect("../index/index.php");
        }
    }

    function redirect($location) {
        $URL = $location;
        echo "<script type='text/javascript'>document.location.href='{$URL}';</script>";
        echo '<META HTTP-EQUIV="refresh" content="0;URL=' . $URL . '">';
        exit();
    }

?>