<?php
    
    include_once('creds.php');


////////////////////////////////////ADDING RECORD//////////////////////////////////////////////////////

    function addNewPage(){
        if(isset($_POST['addPage'])) {
            $db = connection();

            $name = $db->real_escape_string($_POST['name']);
            $navname = $db->real_escape_string($_POST['navname']);
            $jumbohead = $db->real_escape_string($_POST['jumbohead']);
            $jumbosubhead = $db->real_escape_string($_POST['jumbosubhead']);
            $jumbobuttontext = $db->real_escape_string($_POST['jumbobuttontext']);
            $jumbobuttonlink = $db->real_escape_string($_POST['jumbobuttonlink']);
            $stmt = $db->prepare("INSERT INTO tbl_pages (PAGE_NAME) VALUES ('$name')");
            $stmt->bind_param("s", $name);
            $stmt->execute();
            
            print $stmt->error; //to check errors
            $result = $stmt->affected_rows;
            $stmt->close();

            $stmt = $db->prepare("INSERT INTO tbl_nav (TITLE) VALUES ('$navname')");
            $stmt->bind_param("s", $navname);
            $stmt->execute();
            print $stmt->error; //to check errors
            $result = $stmt->affected_rows;
            $stmt->close();

            $stmt = $db->prepare("INSERT INTO tbl_jumbo (TITLE, SUBTITLE, BUTTONTITLE, BUTTONLINK) VALUES ('$jumbohead', '$jumbosubhead', '$jumbobuttontext', '$jumbobuttonlink')");
            $stmt->bind_param("ssss", $jumbohead, $jumbosubhead, $jumbobuttontext, $jumbobuttonlink);
            $stmt->execute();
            print $stmt->error; //to check errors
            $result = $stmt->affected_rows;
            $stmt->close();

            $db->close();
            
            
                                  
                                  

            if ($result > 0) {
                redirect("index.php");
            }
            else {
                //print_r($sql);
                echo "<br><br>";
                echo "An Error has occured";
            }
         }
    }

?>