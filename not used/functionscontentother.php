<?php
    
    include_once('creds.php');

    /////////////////////////////////////GET ITEMS///////////////////////////////////////////////////

    function get_all_contentOther() {
        
        $db = connection();
        $sql = "SELECT * FROM tbl_contentOther";
        $arr = [];

        $result = $db->query($sql);
    
        if(!$result) {
            die("There was an error running the query [".$db->error."] ");
        }

        while ($row = $result->fetch_assoc()) {
            $arr[] = array (
                "id" => $row['ID'],
                "content" => $row['CONTENT'],
                "page_id" => $row['PAGE_ID']
            );
        }

        $json = json_encode($arr);
    
        $result->free();
        $db->close();
        
        return $json;
    }

//////////////////////////////////////////SHOW DATA//////////////////////////////////////////////////////////////////////////////

    function show_contentContentOther($data, $page) {
    
        $array = json_decode($data, True);
        
        $output = "";

        if (count($array) > 0 ) {
            for ($i = 0; $i < count($array); $i++) {
                
                if ($page == "index") {
                    $output .= "<div class=\"col-xs-12 col-md-6\">".$array[$i]['content']."</div>";
                }
                
                if ($page == "admin") {
                    //String for HTML table code
                    //<td>".$array[$i]['category']."</td>
                    $output .= "<div tag=\"emptyspace\" class=\"col-xs-12 col-md-6\">
                                    <p class=\"editquicklink\"><a href=\"editcontentgoogle.php?id=".$array[$i]['id']."\"\>v edit v</a> - <a href=\"deletecontentgoogle.php?id=".$array[$i]['id']."\"\>v delete v</a></p>
                                    ".$array[$i]['content']."
                                </div>";  
                    //add button moved but code is here so can be moved back
                    //<a href=\"addnav.php?id=".$array[$i]['id']."\"\> <p class=\"editnav\">add</p> </a>
                }   
            }
            
            return $output;
        }
        
        else {
            $output .= "<tr><td colspan='5'>No Data Available</td></tr>";
            
            return $output;
        }
    }

////////////////////////////////////SAVING CHANGES///////////////////////////////////////////////////

    function editRecordContentOther() {

        if(isset($_POST['updateContent'])) {
            $db = connection();

            $content = $db->real_escape_string($_POST['content']);
            $id = $db->real_escape_string($_POST['id']);

            $sql = "UPDATE tbl_contentOther SET CONTENT='".$content."' WHERE ID = ".$id."";

            $result = $db->query($sql);

            if ($result == 1) {
                redirect("index.php");
            }
            else {
                print_r($sql);
                return "<br><br>An Error has occured";
                exit();
            }
        }  
    }

////////////////////////////////////DELETING RECORD//////////////////////////////////////////////////////

    function removeSingleRecordContentOther() {
        if(isset($_POST['removeRecord'])) {
            $db = connection();

            $id = $db->real_escape_string($_POST['id']);

            $stmt = $db->prepare("DELETE FROM tbl_contentOther WHERE ID = $id");
            $stmt->bind_param("i", $id);
            $stmt->execute();
            
            print $stmt->error; //to check errors

            $result = $stmt->affected_rows;

            $stmt->close();
            $db->close();

            if ($result == 1) {
                redirect("index.php");
            }
            else {
                print_r($sql);
                echo "<br><br>";
                echo "An Error has occured";
            }
        }
    }

////////////////////////////////////ADDING RECORD//////////////////////////////////////////////////////

    function addRecordContentGoogle(){
        if(isset($_POST['addItem'])) {
            $db = connection();

            $content = $db->real_escape_string($_POST['content']);

            $stmt = $db->prepare("INSERT INTO tbl_contentOther (CONTENT) VALUES ('$content')");
            $stmt->bind_param("s", $content);
            $stmt->execute();

            print $stmt->error; //to check errors

            $result = $stmt->affected_rows;

            $stmt->close();
            $db->close();

            if ($result > 0) {
                redirect("index.php");
            }
            else {
                //print_r($sql);
                echo "<br><br>";
                echo "An Error has occured";
            }
         }
    }

    /////////////////////////////////////DISPLAY ITEMS IN EDIT///////////////////////////////////////////////////

    function displayIdContent() {
        $id = $_GET['id'];
        $array = json_decode(loadContentContentGoogle($id), True);
        return $array[0]['id'];
    }

    function displayContentContent() {
        $id = $_GET['id'];
        $array = json_decode(loadContentContentGoogle($id), True);
        return $array[0]['content'];
    }


//////////////////////////////////////////////LOAD DATA//////////////////////////////////////////////////////////////////////

    function loadContentContentGoogle($id) {

        $db = connection();
        $sql = "SELECT * FROM tbl_contentOther WHERE ID = $id";
        $arr = [];
        
        $result = $db->query($sql);
        
        if(!$result) {
            die("There was an error running the query [".$db->error."] ");
        }
        
        while ($row = $result->fetch_assoc()) {
            $arr[] = array (
                "id" => $row['ID'],
                "content" => $row['CONTENT']
            );
        }
        
        $json = json_encode($arr);
        
        $result->free();
        $db->close();
        
        return $json;        
    }

?>