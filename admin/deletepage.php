<?php include_once('../functions/functions.php');
        include_once('../functions/functionsnav.php');
        include_once('../functions/functionsjumbo.php');
        include_once('../functions/functionsquicklink.php');
        include_once('../functions/functionsotherpage.php');
        include_once('../functions/functionsfooter.php');
    session_start();
    removePage(); ?>

<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Delete nav</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        
        <!-- Custom CSS -->
        <link rel="stylesheet" href="../css/bootstrap-select.min.css" type="text/css" >
        <link rel="stylesheet" href="../css/customstyles.css" type="text/css" >
        <link rel="stylesheet" href="../css/styles.css" type="text/css">
        
    </head>
    <body class="backing">

    <?php 
    if( $_SESSION['login'] == TRUE )
    {
    ?>
        <div class="navbar-custom">
            <div class="container-fluid">
                <nav class="navbar navbar-static-top">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="https://toiohomai.ac.nz/">
                            <img src="../images/toi_logo.png" alt="Toi-Ohomai"></a>
                        </div>
                        <div id="navbar" class="navbar-collapse collapse">
                            <ul class="nav navbar-nav">
                                <li class="down"><a href="index.php" class="">Home</a> </li>
                                <?php echo show_contentNav(get_all_Nav(), "admin"); ?>
                            </ul>
                            <ul class="nav navbar-nav pull-right">
                                <li class="down"><a class="logout-button" href="../index/index.php">Logout</a></li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </div>

        <div class="jumbotron">
            <div class="container">
                <h1 class="display-3"><div class="heading">Delete Page</div></h1>
                <p><div class="sub-heading">Are you sure you want to delete this page and all of the content on it? <br>This action cannot be undone.</div></p>
                <form method='POST' >
                    <input type="hidden" name="id" value="<?php echo displayIDNav(); ?>"/>
                    <h3><?php echo displayNameNav(); ?></h3><br>
                    <input class="btn btn-danger" type="submit" name="removePage" value="Delete Page"/>
                </form>
            </div>
        </div>
    
    <?php  
    }
    else
    {
    ?>
    <div class="container page-header">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-danger">
                    <div class="panel-heading ">
                        <h4>BCS.NET.NZ - ACCESS DENIED</h4>
                    </div>
                    <div class="panel-body customPanel">
                            <h1 class="extraPadding">You do not have access to this page</h1>
                        <h2><a href="../index/login.php"><button class="btn btn-warning" style="width:200px">Go to the login screen</button></a></h2>
                        <h2><a href="../index/index.php"><button class="btn btn-info" style="width:200px">Go back to the home screen</button></a></h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    }
    ?>

        <!-- Content ends here -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../javascript/script.js"></script>

    </body>
</html>